import json
import requests


def dis_loc(s,d):
	url = 'http://localhost:5005/distance'
	data = {'s':s,'d':d}
	headers = {'Content-Type' : 'application/json'}
	r = requests.post(url, data=json.dumps(data), headers=headers)
	return float(r.text)

print dis_loc('Ahmedabad','surat')