from flask import Flask
from flask import request
import requests
import json, re

app = Flask(__name__)


@app.route('/distance', methods=['POST'])
def getDistance():
    if request.method == 'POST':
        jsonRes = request.get_json()
        source = jsonRes['s']
        destination = jsonRes['d']
        print '\n-------------------------------'
        print 'source: ',source
        print 'destination: ',destination
        query = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+source+"&destinations="+destination+"&key=AIzaSyBYslb2HeZtmtkbs9yoGmiKhoyiUyeOlcw"
        jsonOut = requests.get(query)
        myjson = json.loads(jsonOut.text)
        distance = re.findall('\d+',re.sub(',','',myjson['rows'][0]['elements'][0]['distance']['text']))
        # print query
        print 'distance: ',distance
        return distance[0]

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5005, debug=True, threaded=True)
