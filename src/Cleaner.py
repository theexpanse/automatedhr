class Cleaner:

    allpuncs = ['@', '(', ')', '.', '-', ',']

    def stringCleaner(self, string):
        for punc in self.allpuncs:
            string = self.__removepuncs(string, punc)
        return string

    def __removepuncs(self, string, punc):

        string = string.lower()

        if punc in string:
            string = list(string)
            string = [x.replace(punc, ' ') for x in string]
            string = "".join(string)
        string = ' '.join(string.split())
        return string
