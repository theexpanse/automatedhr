import os

import json
import operator
import pandas as pd
import requests
from flask import Flask, request
from math import exp

from edist import ed_lists
from rankExp import make_eq
from rankExp import rank_exp

app = Flask(__name__)


@app.route('/live', methods=["GET"])
def life():
    return "I'm alive!, Ranking Service"

def dis_loc(s,d):
    # print 'source: ',s, ' dest.: ',d
    url = 'http://localhost:5005/distance'
    data = {'s':s,'d':d}
    headers = {'Content-Type' : 'application/json'}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    return float(r.text)


def nearest_neighbours(input_skills):
    data = {'skills':input_skills}
    url = 'http://localhost:5002/knn'
    headers = {'Content-Type' : 'application/json'}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    ID_distance = json.loads(r.text)
    ID_distance = {int(k):v for k,v in ID_distance.items()}
    return ID_distance


def print_attr(l,prtstr):
    print '---------------',prtstr,'------------------'
    str = '%d\t\t>>\t\t%f'
    for k in l:
        print str%(k,l[k])


def rank_distance(l):
    mi = min(l.values())
    ma = max(l.values())
    # for k in l:
    # 	l[k] = 1-l[k]
    if(mi==ma):
        for k in l:
            if(mi==0):
                l[k] = 1
            else:
                print 'min max dis:',ma,mi,l[k]
                l[k] = l[k]/mi
    else:
        for k in l:
            print 'min max dis:',ma,mi,l[k]
            l[k] = 1-(l[k]-mi)/(ma-mi)
    return l

def rank_location(l):
    r_min = 0
    r_max = 100
    mi = min(l.values())
    ma = max(l.values())
    print 'L: ', l
    m, c = make_eq([0, 1],[100, 0.5])
    for k in l:
        if l[k] in range(r_min, r_max):
            print 'In range'
            newL = m*l[k] + c
        else:
            print 'Out of Range'
            newL = exp(r_max - l[k])
        l[k] = newL
    print 'Return L: ', l
    return l


@app.route('/rank/<int:rank_type>',methods=["POST"])				# variables
def rank(rank_type=-1):
    global ref_location
    os.system('clear')
    print '\n\nRank Type: ',rank_type
    # got nearest neighbour IDs from Cluster module
    client_data = request.get_json()
    ref_location = client_data['location']
    input_skills = client_data['skills']
    curr_title = client_data['title']
    exp = client_data['exp']
    client_type = client_data['type']
    print '\n\n Client Data:'
    print 'type:',client_type
    print 'loc:',ref_location
    print 'skills:',input_skills
    print 'exp:',exp
    print '*'*55,' KNN ','*'*55
    url = 'http://localhost:5002/knn'
    headers = {'Content-Type' : 'application/json'}
    r = requests.post(url, data=json.dumps({'skills':input_skills}), headers=headers)
    print r.text
    neighbour_ID_cluster_distance = {int(k):v for k,v in json.loads(r.text).iteritems()}
    print 'cluster distance'
    print neighbour_ID_cluster_distance

    # neighbour_ID_cluster_distance = nearest_neighbours(input_skills)

    # neighbour_ID_cluster_distance = {1:0.3
    # 				,3:0.6
    # 				,10:1
    # 				,20:2.3}
    print '\t\t\t\tgetting clean data on nearest neighbours...'
    url = 'http://localhost:5001/getCleanData'
    data = {'ID':neighbour_ID_cluster_distance.keys()}
    headers = {'Content-Type' : 'application/json'}
    r = requests.post(url,data=json.dumps(data), headers=headers)
    print 'Relevant Job posts Received'
    # print r.text
    n_df = pd.read_json(r.text)
    print n_df.location
    # getting nearest neighbour data frm DE&C module
    # neighbour_data = request.get_json()
    # neighbour_type = neighbour_data['neighbour_type']
    # print 'neighbour type: ',neighbour_type
    # n_df = pd.read_json(neighbour_data['neighbours'])
    # print 'neighbours: \n',n_df
    print '*'*55,' Computing Scores ','*'*55
    if (client_type=='candidate'):
        temp = n_df.req_exp.apply(lambda x: x[0])
        range_exp = temp.max() - temp.min()
        del temp
        neighbour_ID_location_distance = {k:dis_loc(ref_location,v.location) for k,v in n_df.iterrows()}
        print 'Location Distance: ', neighbour_ID_location_distance
        neighbour_ID_location_distance = rank_location(neighbour_ID_location_distance)
        neighbour_ID_cluster_distance = rank_distance(neighbour_ID_cluster_distance)
        weights = [[1.0,0,0,0],[0.7,0.3,0,0],[0.7,0.15,0.15,0],[0.65,0.15,0.15,0.05]]
        WEIGHT_SKILL,WEIGHT_LOCATION, WEIGHT_EXP, WEIGHT_TITLE = weights[rank_type]
        print 'parameter weights (skill, loc, exp, title)',(WEIGHT_SKILL,WEIGHT_LOCATION, WEIGHT_EXP, WEIGHT_TITLE)
        neighbour_rank = []
        neighbour_ID_title_edit_distance = {}
        neighbour_ID_exp_rank = {}
        neighbour_ID_rank = {}
        reject_index=[]
        for k in neighbour_ID_cluster_distance:
            print '\n------------------------------------------key ',k
            ed = ed_lists(curr_title[:],n_df.loc[k].title[:])
            print 'edit dist title: ',ed
            similarity_skills = 1 - ed_lists(input_skills[:],n_df.loc[k].skills[:])
            print 'skills matched: ',similarity_skills*100,'%'
            print 'cluster distance weighted: ',similarity_skills*neighbour_ID_cluster_distance[k]
            if (similarity_skills<0.3):
                print 'REJECTING this job'
                reject_index.append(k)
            neighbour_ID_title_edit_distance[k] = ed
            print 'current exp',exp
            print 'exp range',n_df.loc[k].req_exp

            neighbour_ID_exp_rank[k] = rank_exp(n_df.loc[k].req_exp,exp,0.5)
            rank = WEIGHT_SKILL*neighbour_ID_cluster_distance[k] \
                    + WEIGHT_LOCATION*neighbour_ID_location_distance[k] \
                    + WEIGHT_EXP*neighbour_ID_exp_rank[k] \
                    + WEIGHT_TITLE*(0 if ed>0.5 else 1)
            neighbour_rank.append(rank*10)
            neighbour_ID_rank[k] = rank*10
        # print neighbour_rank
        print_attr(neighbour_ID_cluster_distance,'Cluster Distance')
        print_attr(neighbour_ID_location_distance,'Location Distance')
        print_attr(neighbour_ID_title_edit_distance,'Title Edit Distance')
        print_attr(neighbour_ID_exp_rank,'Experience Rank')
        print_attr(neighbour_ID_rank,'Final Rank')

        n_df['Rank'] = pd.Series([0 for i in range(n_df.shape[0])], index=n_df.index)
    else:
        pass
    # print n_df.loc[n_df.Rank.sort_values(ascending=False).index].to_json()
    for i in neighbour_ID_rank:
        n_df.loc[i,'Rank'] = neighbour_ID_rank[i]
    if(reject_index):
        print 'droping rejected jobs... IDs=',reject_index
        n_df.drop(reject_index,inplace=True)
    ndfjson = n_df.loc[n_df.Rank.sort_values(ascending=False).index].to_json()
    ndfjson = json.loads(ndfjson)
    if (n_df.shape[0]==0):
        return 'None!!'
    sorted_x = sorted(neighbour_ID_rank.items(), key=operator.itemgetter(1),reverse=True)
    ndfjson['IDs'] = [k for k,v in sorted_x]
    print 'IDs',ndfjson['IDs']
    print 'Rank',ndfjson['Rank']
    n_df.to_csv('reccomended_jobs_inc.csv')
    return json.dumps(ndfjson)
    # return n_df.loc[n_df.Rank.sort_values(ascending=False).index].to_html()


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5006,debug=True,threaded=True)
