#printf "$@"
#printf "$#"

if [ $# -eq 1 ] 
then
  args="$1"
  printf "\n\n\e[1;96mArguments are Enabled...\e[0m"
  printf "Your Arguments : $args"
else
	printf "\n\n\e[1;31mPlease provide Commit Message as an Argument!!\e[0m \n"
	exit 101
fi


printf "\nYour Commit Message : $args"

printf "\n\nDid you pulled the branch? [y/n] "
read ch

if [ $ch == 'y' ]
then
	git add .
	git commit -m"$args"
	git push origin master
else
	printf "\nTry running download.sh"
	printf "\n\e[1;31mQuiting...\e[0m \n"
fi
