import requests,json
from network_info import *
from flask import Flask, request, render_template

app = Flask(__name__)
headers = {'Content-Type' : 'application/json'}

@app.route('/', methods=["GET"])
def homePage():
    return '<h2>Hello, from Web Interface!</h2>'

@app.route('/live', methods=["GET"])
def checkLife():
    return "I'm Alive, says Web Interface!"

@app.route('/upload', methods=["GET"])
def uploadData():
    url = 'http://'+domain+broker_subnet+':'+broker_port+'/listen'

    dataX = json.dumps({
        "Name" : "Saumil",
        "Location" : "Ahmedabad",
        "Education" : {
            "Qualification" : "B.Tech",
            "Institute" : "SEAS, AU",
            "School-Duration" : "2013-2017"
            },
        "Work-Experience" : {
            "Job Title":"Project Assistant",
            "Company" : "ezDI India",
            "Job-Duration" : "1 year",
            "Job-Description" : "Software Devloper to create a matchmaking platform"
            },
        "Skills" : ["java", "python", "c", "semantic-web"],
        "Additional-Info" : "additionalInfoX",
        "Resume-Summary" : "resSummaryX"})
    data = { "callType" : "send",
            "MethodName" : "cleanData",
            "payload" : dataX}

    r = requests.post(url, data=json.dumps(data), headers=headers)
    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text

    print "WebInterface Says : "+r.text
    return "WebInterface Says : "+r.text

if __name__ == '__main__':
    app.run(port=int(webint_port), threaded=True)
