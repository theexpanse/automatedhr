import sys, web, json, requests
print 'Initializing...'

urls = (
    '/', 'platform',
    '/jobId', 'jobId',
    '/.*', 'other'

    )

render = web.template.render('templates/')

app = web.application(urls, globals())

my_form = web.form.Form(
    web.form.Textbox(name='Candidate Name : ',value='Saumil Shah', class_='tfield', id='tfield1'),
    web.form.Textbox(name='Skills : ',value='core-java, hibernate, java, linux, oracle, sql, tomcat, uml, webservices', class_='tfield', id='tfield2'),
    web.form.Textbox(name='Current Title : ',value='Application Developer', class_='tfield', id='tfield3'),
    web.form.Textbox(name='Location : ',value='Ahmedabad', class_='tfield', id='tfield4'),
    web.form.Textbox(name='Experience : ',value='4', class_='tfield', id='tfield5'),

    # web.form.Textbox(name='Candidate Name2 : ',value='Saumil Shah', class_='tfield', id='tfield1'),
    # web.form.Textbox(name='Skills2 : ',value='core-java, hibernate, java, linux, oracle, sql, tomcat, uml, webservices', class_='tfield', id='tfield2'),
    # web.form.Textbox(name='Current Title2 : ',value='Application Developer', class_='tfield', id='tfield3'),
    # web.form.Textbox(name='Location2 : ',value='Ahmedabad', class_='tfield', id='tfield4'),
    # web.form.Textbox(name='Experience2 : ',value='4', class_='tfield', id='tfield5'),
    #
    # web.form.Textbox(name='Candidate Name3 : ',value='Saumil Shah', class_='tfield', id='tfield1'),
    # web.form.Textbox(name='Skills3 : ',value='core-java, hibernate, java, linux, oracle, sql, tomcat, uml, webservices', class_='tfield', id='tfield2'),
    # web.form.Textbox(name='Current Title3 : ',value='Application Developer', class_='tfield', id='tfield3'),
    # web.form.Textbox(name='Location3 : ',value='Ahmedabad', class_='tfield', id='tfield4'),
    # web.form.Textbox(name='Experience3 : ',value='4', class_='tfield', id='tfield5'),
    #
    # web.form.Textbox(name='Candidate Name4 : ',value='Saumil Shah', class_='tfield', id='tfield1'),
    # web.form.Textbox(name='Skills4 : ',value='core-java, hibernate, java, linux, oracle, sql, tomcat, uml, webservices', class_='tfield', id='tfield2'),
    # web.form.Textbox(name='Current Title4 : ',value='Application Developer', class_='tfield', id='tfield3'),
    # web.form.Textbox(name='Location4 : ',value='Ahmedabad', class_='tfield', id='tfield4'),
    # web.form.Textbox(name='Experience4 : ',value='4', class_='tfield', id='tfield5'),
)

# clean_data_url = 'http://10.20.24.48:5001'
# clean_data_url = 'http://localhost:5001'
clean_data_url = 'http://2f3c1b6a.ngrok.io'

# rank_url = 'http://10.20.24.48:5006'
# rank_url = 'http://localhost:5006'
rank_url = 'http://8b781339.ngrok.io'

# job_url = 'http://10.20.24.48:5006'
job_url = 'http://localhost:44000/job/'
# job_url = 'http://da3cbee9.ngrok.io'

class other:
    def GET(self):
        return "Invalid Path Requested in GET"

    def POST(self):
        return "Invalid Path Requested in POST"


class jobId:
    def GET(self):
        print "In Job GET"

        print "\nD"
        print web.input()
        print web.input(_method='get')
        print web.data()

        return web.input()
        # return web.input(_method='get')

        # return "Request Received!"

    def POST(self):

        print "In Job POST"

        print "\nD"
        print web.input()
        print web.input(_method='get')
        print web.data()

        jobID = web.input()['id']
        print "jobID : ",jobID

        r = requests.post(job_url+jobID, headers={'Content-Type' : 'application/json'})
        print "R : ",r.text

        job_data = json.loads(r.text)
        print "job_data : ",job_data

        # return json.dumps(web.data())
        return json.dumps(job_data)

class platform:
    def GET(self):
        form = my_form()
        return render.platform(form, "Your text goes here...")

    def POST(self):
        form = my_form()
        form.validates()

        s = ''
        sz = ''
        x = ''
        typeX = ''

        try:
            sz = form.value['tfield']
            x = str(sz)
            typeX = x.split('%')[1]
            print 'Content : ',x, type(x)
            print 'Type : ', typeX

        except:
            print 'Non-Type : Unexpected'

        if typeX == '1' or typeX == '2' or typeX == '3' or typeX == '4':

            Candidate = [[]]
            Candidate[0] = x.split(':')[0]
            Candidate += [{'Skills': x.split(':')[1].split('@')[0].split('^')[0].replace(", ", ",").split(',') }]
            Candidate += [x.split('@')[1].split('^')[0]]
            Candidate += [x.split('^')[1].split('#')[0]]
            Candidate += [x.split('^')[1].split('#')[1].split('%')[0]]
            print 'In App : ',Candidate

            print "\n\n*****"

            CandidateX = {
            'skills': x.split(':')[1].split('@')[0].split('^')[0].replace(", ", ","),
            'title': x.split('@')[1].split('^')[0],
            'exp': int(x.split('^')[1].split('#')[1].split('%')[0]),
            'location': x.split('^')[1].split('#')[0]
            }
            print "\n\nCandidateX : ",CandidateX


            print '\nCleaning Data...'
            r = requests.post(clean_data_url+'/clean_client_data', data=json.dumps({k:CandidateX[k] for k in ['skills','title']}), headers={'Content-Type' : 'application/json'})
            print r.text
            clean_data = json.loads(r.text)

            CandidateX['skills'] = clean_data['skills']
            CandidateX['title'] = clean_data['title']
            CandidateX['type'] = 'candidate'
            print '\n\nData : ',CandidateX

            print '\nFetching Ranks...'

            # r = requests.post(rank_url+'/rank', data=json.dumps(CandidateX), headers={'Content-Type' : 'application/json'})
            print (int(typeX)-1)
            r = requests.post(rank_url+'/rank/'+str(int(typeX)-1), data=json.dumps(CandidateX), headers={'Content-Type' : 'application/json'})
            print 'Data : ',r.text

            return json.dumps(r.text)

        else:
            print "Invalid Type...!"
            return 'Caused Problems...'

if __name__ == '__main__':
    app.run()
    print 'Exiting...!'
