import json, requests, time
from network_info import *

headers = {'Content-Type' : 'application/json'}

""" Method Calls for Broker """
def checkLifeWeb():
    url = 'http://'+domain+webint_subnet+':'+webint_port+'/live'
    r = requests.get(url)

    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text


def uploadData():
    url = 'http://'+domain+webint_subnet+':'+webint_port+'/upload'
    r = requests.get(url)
    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text

""" Method Calls for Broker """
def checkLifeBroker():
    url = 'http://'+domain+broker_subnet+':'+broker_port+'/live'
    r = requests.get(url)

    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text

""" Method Calls for Data E&C Service """
def checkLifeDEC():
    url = 'http://'+domain+dec_subnet+':'+dec_port+'/live'
    r = requests.get(url)

    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text

""" Method Calls for Cluster Service """
def checkLifeCluster():
    url = 'http://'+domain+cluster_subnet+':'+cluster_port+'/live'
    r = requests.get(url)

    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text


""" Method Calls for Ontology Service """
def checkLifeOntoService():
    url = 'http://'+domain+onto_subnet+':'+onto_port+'/live'
    r = requests.get(url)

    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text


""" Method Calls for Ranking Service """
def checkLifeRankingService():
    url = 'http://'+domain+rank_subnet+':'+rank_port+'/live'
    r = requests.get(url)

    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text


while True:

    print "\nSystem Menu:"
    print "W. Call WI"
    print "1. Call Data E&C"
    print "2. Call Cluster"
    print "3. Call OntoService"
    print "4. Call RankingService"
    print "0. Exit"

    case = raw_input("\n\nEnter your choice...")

    if case == "w" or case == "W":
        print "\nWeb Interface:"
        print "1. Check WebInterface"
        print "2. Check Broker"
        print "3. Call cleanData"
        print "",
        subcase = raw_input("Enter your subchoice...")


        if subcase == "1":
            checkLifeWeb();
        elif subcase == "2":
            checkLifeBroker();
        elif subcase == "3":
            uploadData();
        else:
            print "Invalid Choice, WebInterface"

    elif case == "1":
        print "\nData E&C Menu:"
        print "1. Check Data E&C"
        print "2. Call cleanData"
        print "3. Call sendSkillData"
        print "4. Call sendCleanedData"
        print "",
        subcase = raw_input("Enter your subchoice...")

        if subcase == "1":
            checkLifeDEC();
        elif subcase == "2":
            cleanData();
        elif subcase == "3":
            sendSkillData();
        elif subcase == "4":
            sendCleanedData();
        else:
            print "Invalid Choice, Data E&C-Service"

    elif case == "2":
        print "\nCluster Menu:"
        print "1. Check Cluster Service"
        print "2. Call Clusterize"
        print "3. Call KNN"
        print "4. Call sendIDs"
        print "5. Call getSkillData"
        print "",
        subcase = raw_input("Enter your subchoice...")

        if subcase == "1":
            checkLifeCluster();
        elif subcase == "2":
            clusterizeData();
        elif subcase == "3":
            findKNN();
        elif subcase == "4":
            sendIDs();
        elif subcase == "5":
            getSkillData();
        else:
            print "Invalid Choice, ClusterService"

    elif case == "3":
        print "\nOntology Menu:"
        print "1. Check Ontology Service"
        print "2. Call getWeights"
        print "",
        subcase = raw_input("Enter your subchoice...")

        if subcase == "1":
            checkLifeOntoService();
        elif subcase == "2":
            getWeights();
        else:
            print "Invalid Choice, OntoService"

    elif case == "4":
        print "\nRanking Menu:"
        print "1. Check RankingService"
        print "2. Call getIDs"
        print "3. Call getCleanedData"
        print "",
        subcase = raw_input("Enter your subchoice...")

        if subcase == "1":
            checkLifeRankingService();
        elif subcase == "2":
            getIDs();
        elif subcase == "3":
            getCleanedData();
        else:
            print "Invalid Choice, RankingService"

    elif case == "0":
        break;

print "\n\nBroker Terminated...",
