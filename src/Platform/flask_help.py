from flask import Flask, request, render_template, jsonify

app = Flask(__name__)

@app.route('/')									# maps url to rturn statement of the function, @ is decorator
@app.route('/<user>')
def index(user=None):
	return render_template('user.html',user=user)


@app.route('/tuna')								# maps url to rturn statement of the function, @ is decorator
def hi():
	return '<h2> tuna is good!</h2>'			# we can also return HTML


@app.route('/display/<username>')				# variables, here username will be string, not int or anything
def user(username):
	return '<h2>hi %s!</h2>'%username


@app.route('/post/<int:post_id>')				# variables
def post(post_id):
	return '<h2>post id is %d!</h2>'%post_id


# for POST methods
@app.route('/bacon',methods=['GET', 'POST'])
def bacon():
	if request.method == 'POST':
		return 'You are using post!'
	else:
		return 'You are using GET'


# use of templates
@app.route('/profile/<name>')
def profile(name):
	return render_template('profile.html',name=name)


# passing objects
@app.route('/shopping')
def shopping():
	food = ['cheese','Tuna','Mango']
	return render_template('shopping.html',food=food)


@app.route('/receive', methods=["POST"])
def receive():

    if request.method == "POST":
        json_dict = request.get_json()

        stripeAmount = json_dict['stripeAmount']
        stripeCurrency = json_dict['stripeCurrency']
        stripeToken = json_dict['stripeToken']
        stripeDescription = json_dict['stripeDescription']

        data = {'stripeAmountRet': stripeAmount, 'stripeCurrencyRet': stripeCurrency, 'stripeTokenRet': stripeToken, 'stripeDescriptionRet': stripeDescription}
        print 'in receive method'
        return jsonify(data)
    else:

        return """<html><body>
        Something went horribly wrong
        </body></html>"""



if __name__ == '__main__':
	app.run(port=5000,debug=True)							# debug=True: developer mode
Add Comment Collapse
