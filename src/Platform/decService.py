import requests,json, time
from network_info import *
from flask import Flask, request, render_template

app = Flask(__name__)
headers = {'Content-Type' : 'application/json'}

@app.route('/', methods=["GET"])
def homePage():
    return '<h2>Hello, from DE&C Service!</h2>'

@app.route('/live', methods=["GET"])
def checkLife():
    return "I'm Alive, says DE&C Service!"

@app.route('/cleanData', methods=["POST"])
def cleanData():
    print "\n\nIn cleanData..."
    """Rx Data"""
    rX = request.get_json()
    print "RX : ",rX

    print "Data Received!"

    url = 'http://'+domain+broker_subnet+':'+broker_port+'/done'
    data = { "callType" : "done",
            "MethodName" : "uploadData"}
    print url
    r = requests.post(url, data=json.dumps(data), headers=headers)
    if r.status_code != 200:
        print "\nErr Code : ",r.status_code
    print "Got : ",r.text


    """Processing"""
    print "Processing..."
    time.sleep(4)
    print "Processed"
    # print "\n"
    # x=raw_input("Let's see what happens...!!!?")

    return "Data Cleaned...!"

if __name__ == '__main__':
    app.run(port=int(dec_port), threaded=True)
