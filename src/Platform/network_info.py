webint_port     = "5005"
broker_port     = "5000"
dec_port        = "5001"
cluster_port    = "5002"
onto_port       = "5003"
rank_port       = "5004"

domain          = "127.0."

webint_subnet   = "0.1"
broker_subnet   = "0.1"
dec_subnet      = "0.1"
cluster_subnet  = "0.1"
onto_subnet     = "0.1"
rank_subnet     = "0.1"

# task = "live"
# url = 'http://'+domain+subnet+':'+port+'/'+task
