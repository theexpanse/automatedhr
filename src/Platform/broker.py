import requests,json
from network_info import *
from flask import Flask, request, render_template

app = Flask(__name__)
headers = {'Content-Type' : 'application/json'}

@app.route('/', methods=["GET"])
def homePage():
    return '<h2>Hello, from Broker!</h2>'

@app.route('/live', methods=["GET"])
def checkLife():
    return "I'm Alive, says Broker!"

@app.route('/listen', methods=["POST"])
def listen_post():
    print "\n\nIn listen_post..."
    rX = request.get_json()

    callType = rX["callType"]

    if callType == "get":
        methodCall = rX["MethodName"]
        return "getting"
    elif callType == "send":
        methodCall = rX["MethodName"]
        methodPayload = rX["payload"]

        if methodCall == "cleanData":
            url = 'http://'+domain+dec_subnet+':'+dec_port+'/'+methodCall

            r = requests.post(url, data=json.dumps(methodPayload), headers=headers)

            if r.status_code != 200:
                print "\nErr Code : ",r.status_code
            print "Got : ",r.text

            print "Broker Says : "+r.text
            return "Broker Says : "+r.text
        elif methodCall == "sendSkillData":
            return "sendSkillData"
        else:
            return "No Method, Alas!"
    elif callType == "done":
        return "Done!"
    else:
        return "No such CallType, Alas!"

@app.route('/done', methods=["POST"])
def done_post():
    print "\n\nIn done_post..."
    rX = request.get_json()
    print "RX : ",rX

    callType = rX["callType"]
    if callType == "done":
        return "Broker says DONE!"
    else:
        return "Broker says NOT-DONE!!!"

if __name__ == '__main__':
    app.run(port=int(broker_port), threaded=True)
