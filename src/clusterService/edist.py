from nltk import memoize
from nltk.metrics.distance import edit_distance
import datetime as d

@memoize
def edist(a,b):
	if len(a)==0:
		return len(b)
	if len(b)==0:
		return len(a)
	delta = 0 if a[-1]==b[-1] else 1
	return min(edist(a[:-1],b[:-1])+delta, 
				edist(a,b[:-1])+1, 
				edist(a[:-1],b)+1)

def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(S[i-c+1:i+1])
                elif c == longest:
                    lcs_set.add(S[i-c+1:i+1])

    return lcs_set

def ed(a,b):
	l = max(len(a),len(b))
	e = edit_distance(a,b)
	return round(e/float(l),2)

def ed_lists(l1,l2):


	m = len(l1)
	n = len(l2)
	ed_list = []
	l1,l2 = (l1,l2) if m<n else (l2,l1)
	for token1 in l1:
		token_ed = map(lambda a: ed(a,token1),l2)
		min_ed = min(token_ed)
		mapped_token = l2[token_ed.index(min_ed)]
		l2.remove(mapped_token)
		ed_list.append(min_ed)
		print token1,'-->',mapped_token
	print 'remaining tokens..',l2
	ed_list+=[1.0]*len(l2)
	print ed_list
	return sum(ed_list)/max(m,n)

		


# l1 = ['core-java', 'db2', 'hibernate', 'j2ee', 'java', 'oracle', 'servlets', 'struts', 'troubleshooting']
# l2 = ['core-java', 'hibernate', 'j2ee', 'java', 'linux', 'sql', 'tomcat', 'uml', 'weblogic']
# str1='application java developer'.split(' ')
# str2 = 'devloper aaplication python java'.split(' ')
# st = d.datetime.now()
# print ed(str1,str2)
# print ed_lists(l1[:],l2[:]) # pass by value
# print (d.datetime.now()-st).total_seconds(),'seconds'
