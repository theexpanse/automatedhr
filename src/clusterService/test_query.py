import json

import requests
from flask import Flask

app = Flask(__name__)

@app.route('/')									# maps url to rturn statement of the function, @ is decorator		
@app.route('/<user>')
def index(user=None):
    print user
    return "Hi there!"

@app.route('/run_testcluster')
def post_json():
    url = 'http://localhost:5002/clustering'
    data = {1: ['java','android','html','css','spring','spring-mvc'],
    		2: ['java','android','python','numpy','scipy'],
    		3: ['html','css','machine-learning','artificial-intelligence']}
    headers = {'Content-Type' : 'application/json'}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    # return json.dumps(r.json(), indent=4)
    return r.text

@app.route('/run_testquery')
def test_query():
    url = 'http://localhost:5003/getWeights'
    data = {'query':['java','android','html','css','spring','spring-mvc']}
    headers = {'Content-Type' : 'application/json'}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    # return json.dumps(r.json(), indent=4)
    return r.text


if __name__ == '__main__':
    app.run(port=5000,debug=True)							# debug=True: developer mode