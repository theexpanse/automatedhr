import csv, pickle, requests, json, time, pickle, os
from flask import Flask, request,jsonify
import pandas as pd
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from Sillhoutte import *
from edist import ed_lists

feature_skills = set()
cluster_df = pd.DataFrame()
skills_cluster_model = MiniBatchKMeans()
# PATH_DATA = '/home/axat/BTP2/automatedhr/res/Data/'
PATH_DATA = '../../res/Data/'
n_clusters = 5
app = Flask(__name__)
def enumerate(sequence, start=0):
    n = start
    for elem in sequence:
        yield n, elem
        n += 1

@app.route('/live', methods=["GET"])
def life():
    return "I'm alive!, Cluster Service"

def query(input_skills):
    url = 'http://localhost:5003/getWeights'
    # print 'input skills',input_skills
    data = {'query':input_skills}
    # print data
    headers = {'Content-Type' : 'application/json'}
    r = None
    r = requests.post(url, data=json.dumps(data), headers=headers)
    # return json.dumps(r.json(), indent=4)
    # print r
    # print r.text
    return r.text

@app.route('/knn', methods=["POST"])
def knn():
    data = request.get_json()
    dis = []
    input_skills = data['skills']
    ed_dist = []
    cols = cluster_df.iloc[:,:-1].columns
    print 'got: ',input_skills
    # for i,v in cluster_df.ix[:,:-1].apply(lambda x: x > 0).apply(lambda x: list(cols[x.values]), axis=1).iteritems():
    #     ed_dist.append(ed_lists(input_skills[:],v[:]))
    weights = json.loads(query(input_skills))
    print weights
    y = [weights.get(skill,0) for skill in feature_skills]
    for i in cluster_df.index:
        dis.append(sum((cluster_df.ix[i,:-1].values-y)**2)**(0.5))

    dist = pd.Series(dis, index=cluster_df.index)
    print dist.sort_values()[:10].to_json()
    # print dis.ix[48588]
    # print cluster_df.ix[48588,input_skills]
    return dist.sort_values()[:10].to_json()

@app.route('/sendIDs', methods=["POST"])
def sendIDs():
    pass

@app.route('/getSkillData', methods=["POST"])
def getSkillData():
    data = request.get_json()
    print data
    temp_df = pd.read_json(data)
    print temp_df.columns
    return 'received data!'

def sillhoutte_plot():
    krange = range(5,20)+range(45,56)
    X = cluster_df.ix[:,:-1].values
    Sillhoutte(X,krange)

def savefiles():
    cluster_df.to_pickle('JP_clusterDF')
    pickle.dump(skills_cluster_model, open('JP_k-means_Model', 'wb'))
    pickle.dump(feature_skills, open('feature_skills', 'wb'))
    print 'cluster details saved!'


def loadfiles():
    global cluster_df, skills_cluster_model, feature_skills
    if(os.path.isfile('JP_clusterDF') and os.path.isfile('JP_k-means_Model') and os.path.isfile('feature_skills')):
        cluster_df = pd.read_pickle('JP_clusterDF')
        skills_cluster_model = pickle.load(open('JP_k-means_Model','rb'))
        feature_skills = pickle.load(open('feature_skills','rb'))
        print 'all files loaded successfully...'
        print 'clusterdf size: ',cluster_df.shape
        print 'k means model inertia: ',skills_cluster_model.inertia_
        fl = len(feature_skills)
        print 'feature size: ',fl
        average_feature_len = sum((len(skill) for skill in feature_skills))/float(fl)
        print 'Average feature length: ',average_feature_len
    else:
        print 'files not read'


def gscatter(x,labels):
    n = len(set(labels))
    label_color = [cm.spectral(float(l)/n) for l in labels]
    plt.scatter(x[:,0],x[:,1], c=label_color)
    plt.savefig('T-SNE_result.png')
    plt.close()

def tsne_visualize(labels):
    model = TSNE(n_components=2, random_state=0)
    data_2d = model.fit_transform(cluster_df.ix[:, :-1].values)
    gscatter(data_2d,labels)
    # plt.scatter(data_2d[:,0],data_2d[:,1])
    # plt.savefig('T-SNE_result.png')
    # plt.close()

# def vector(input_skills, weights):
#     return [0 if skill not in input_skills else weights[skill] for skill in feature_skills]

@app.route('/clustering', methods=["POST"])
def clusterize():
    global feature_skills, skills_cluster_model, cluster_df
    # initializing feature skills, so everytime new data comes, features will be updated accordingly
    feature_skills = set()

    # format {'<id>':[u'<str>,...],...}
    data = request.get_json()

    # constructing features
    # for skills in data.values():
    #     feature_skills.update(skills)
    # feature_skills = sorted(feature_skills)
    # n_samples = len(data)
    # n_features = len(feature_skills)

    # initialize data frame
    # feature_skills = feature_skills.update(['lable'])
    weights = {}
    for key in data:
        input_skills = data[key]
        # print int(key)
        weights[int(key)] = json.loads(query(input_skills))
        # print  'returned weights',weights
        # cluster_df.loc[int(key)] = [0 if skill not in input_skills else weights[skill] for skill in feature_skills]

    for key in weights:
        feature_skills.update(weights[int(key)].keys())
    feature_skills = sorted(feature_skills)
    n_samples = len(data)
    n_features = len(feature_skills)
    cluster_df = pd.DataFrame(columns=feature_skills)

    for key in data:
        input_skills = data[key]
        cluster_df.loc[int(key)] = [weights[int(key)].get(skill,0) for skill in feature_skills]
    # skills_cluster_model = KMeans(n_clusters=n_clusters,max_iter=1000,init='k-means++')
    # lables = skills_cluster_model.fit_predict(cluster_df.ix[:,:].values)
    skills_cluster_model = MiniBatchKMeans(init='k-means++', n_clusters=n_clusters, batch_size=100,
                      n_init=10, max_no_improvement=10, verbose=0)
    lables = skills_cluster_model.fit_predict(cluster_df.ix[:,:].values)
    cluster_df['lable'] = pd.Series(lables, index=cluster_df.index)
    print_clusteredSkills()
    sillhoutte_plot()
    tsne_visualize(lables)
    savefiles()
    return 'clustering finished!'

def print_clusteredSkills():
    cols = cluster_df.iloc[:,:-1].columns
    print len(cluster_df.index)
    print cols[-1]
    # deleting all the contents in existing file
    f = open('JPclusters.csv','w')
    f.close()

    for i in range(n_clusters):
        df2 = cluster_df[cluster_df.lable==i]
        df2 = df2.iloc[:,:-1]
        bt = df2.apply(lambda x: x > 0, raw=True)
        ct = bt.apply(lambda x: str(i)+':  '+', '.join(list(cols[x.values])), axis=1)
        with open('JPclusters.csv', 'a') as f:
            f.write('\n\n')
            ct.to_csv(f, header=False)





if __name__ == '__main__':
    loadfiles()
    app.run(host='0.0.0.0',port=5002,debug=True,threaded=True)                            # debug=True: developer mode
# construct_graph('classes_v2.csv',True)
# q = ['java','android','html','css','spring','spring-mvc','bobbo']
# print weightify(q)
# subgraph = {k:[a for a in skill_graph[k]if a in query] for k in query if k in keys}
# print subgraph
