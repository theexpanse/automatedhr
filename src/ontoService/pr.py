import rdflib, time, json
from weightify import *
from flask import Flask, request,jsonify
from rdflib.plugins.sparql import prepareQuery
app = Flask(__name__)
PATH_DATA = '../../res/Data/'
g = ''

@app.route('/getWeights', methods=["POST"])
def myQuery():
    global g
    ip_skills = request.get_json()['query']
    print 'input skills: ',ip_skills
    Q = """PREFIX ss:<http://www.semanticweb.org/btp1/stackoverflow_skill_ontology#>

            SELECT ?g ?dist
            WHERE
            {   ss:android rdfs:subClassOf ?g 
                OPTION(transitive, T_DISTINCT, t_in(?object1), t_out(?object2), t_max(15),t_step ('step_no') as ?dist, T_DIRECTION 1 ).
            } """
    t1 = time.time()
    rows = g.query(Q)
    print 'Time Taken: ',time.time()-t1,' seconds'
    j = rows.serialize(format="json")
    print j
    JSON = json.loads(j)
    return 'hi'

if __name__ == '__main__':
    g = rdflib.Graph()
    g.load(PATH_DATA+'skill_v2.rdf')
    app.run(host='0.0.0.0',port=5003,debug=True,threaded=True)                          # debug=True: developer mode

ip_skills = ['android','java', 'spring', 'spring-mvc', 'hibernate','python','numpy','scipy','html','parrot','outlier']
# weights = Query(ip_skills,g)
# print weights
