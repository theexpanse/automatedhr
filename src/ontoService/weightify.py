def weightify(tree,c):
	adj = {}
	weight = {}
	skillsFound = []
	c +=['Owl-Thing']
	print '\n'
	if (tree!=[[]]):
	# making adjacency list
		for row in tree:
			print row
			if(adj.get(row[0],None)==None):
				adj[row[0]] = set([row[1]])
			else:
				adj[row[0]].add(row[1])

		print adj
		#assigning weights to all non leaf node
		for k in adj:
			weight[k] = height(adj,k)


		skillsFound = [skill for sublist in tree for skill in sublist] # skills which exists in ontology

		w = {k:1 for k in skillsFound if k not in weight} # weight of non-leaf nodes 
		weight.update(w)

	# assigning weight to skills which does not exists in ontology
	notfound = {k:-1 for k in c if k not in skillsFound}
	weight.update(notfound)
	del weight['Owl-Thing']
	return weight

def height(d,key):
	a = []
	if(d.get(key,None)==None):
		return 1
	for s in d[key]:
		a.append(height(d,s))
	return max(a)+1

# tree = [['java','spring'],
# 		['spring','spring-mvc'],
# 		['java','android'],
# 		['python','scipy'],
# 		['scipy','numpy'],
# 		['spring','hibernate'],
# 		['programming','python'],
# 		['programming','java'],
# 		['java','python']]

# skills = ['java','spring','spring-mvc','android','scipy','numpy','hibernate','python','programming','outlier']

# w = weightify(tree,skills)
# print w