import csv
import pickle
from flask import Flask, request,jsonify
app = Flask(__name__)
# PATH_DATA = '/home/axat/BTP2/automatedhr/res/Data/'
PATH_DATA = '../../res/Data/'
skill_graph = {}
skill_set = set()
keys = []
query = []

@app.route('/live', methods=["GET"])
def life():
	return "I'm alive!, Ontology Service"

def construct_graph(classfile,saveFile=False):
	global skill_graph, skill_set, keys
	with open(PATH_DATA+classfile) as csvfile:
	    creader = csv.reader(csvfile,delimiter=',')
	    for row in creader:
	        Class = row[0]
	        superClass = row[1]
	        skill_set.update([Class,superClass])
	        if(skill_graph.get(superClass,None)==None):
	            skill_graph[superClass] = set([Class])
	        else:
	            skill_graph[superClass].update([Class])
	keys = skill_graph.keys()
	if(saveFile==True):
		print 'saving ontology graph to '+PATH_DATA+'skillGraph.pkl'
		with open(PATH_DATA+'skillGraph.pkl', 'wb') as f:
			pickle.dump(skill_graph, f, pickle.HIGHEST_PROTOCOL)


def load_skillGraph():
	global skill_graph, skill_set, keys
	with open(PATH_DATA+'skillGraph.pkl', 'rb') as f:
		skill_graph = pickle.load(f)
	keys = skill_graph.keys()
	skill_set.update(skill_graph.keys())
	for l in skill_graph.values():
		skill_set.update(l)


def height(k):
	if k in keys:
		# print k
		try:
			return max(map(height,(child for child in skill_graph[k] if child in query)))+1
		except ValueError:
			return 1
	else:
		return 1



@app.route('/getWeights', methods=["POST"])
def getWeights():
	q = request.get_json()['query']
	# q = q['query']
	global query
	query = []
	print 'query received \n',q
	not_found = []
	for skill in q:
		if skill in skill_set:
			query.append(skill)
			continue
		not_found.append(skill)
	weights={}
	for skill in query:
		weights[skill] = height(skill)
	weights.update({skill:1 for skill in not_found})
	print 'Weights',weights
	return jsonify(weights)


@app.route('/get_DerivedWeights', methods=["POST"])
def getDericedWeights():
	q = request.get_json()['query']
	# q = q['query']
	global query
	query = []
	print 'query received \n',q
	not_found = []
	for skill in q:
		if skill in skill_set:
			query.append(skill)
			continue
		not_found.append(skill)
	weights={}
	for skill in query:
		weights[skill] = height(skill)
	weights.update({skill:1 for skill in not_found})
	print 'Weights',weights
	return jsonify(weights)


if __name__ == '__main__':
	
	load_skillGraph()
	app.run(host='0.0.0.0',port=5003,debug=True,threaded=True)							# debug=True: developer mode
# construct_graph('classes_v2.csv',True)
# q = ['java','android','html','css','spring','spring-mvc','bobbo']
# print weightify(q)
# subgraph = {k:[a for a in skill_graph[k]if a in query] for k in query if k in keys}
# print subgraph
