from SPARQLWrapper import SPARQLWrapper, JSON
from nltk import FreqDist
from flask import Flask, request,jsonify
# input_skills = ['cq5','java-ee','css','jquery']
app = Flask(__name__)
input_skills = ['gorm','hibernate','android-studio','groovy']
graph = {}
parentTree = {}
childcount = {}
vertexweight = {}
traversed = set()


@app.route('/live', methods=["GET"])
def life():
	return "I'm alive!, Ontology Service"


def getParentTree(skill):

	sparql = SPARQLWrapper("http://10.20.24.48:8890/sparql")
	queryskill = '<http://www.semanticweb.org/btp1/stackoverflow_skill_ontology#{0}>'.format(skill)
	sparql.setQuery("""SELECT ?g ?dist from <http://localhost:8890/DAV>
				WHERE{
					"""+queryskill+""" rdfs:subClassOf ?g OPTION(transitive, T_DISTINCT, t_in(?object1), t_out(?object2), t_max(15),t_step ('step_no') as ?dist, T_DIRECTION 1 ).}
	""")
	sparql.setReturnFormat(JSON)
	results = sparql.query().convert()
	lables = results['head']['vars']
	parent_dist = {}
	for result in results["results"]["bindings"]:
		parent_dist[result['g']["value"].split('#')[1]] = int(result['dist']["value"])
	return parent_dist

def getParents(skill):
	sparql = SPARQLWrapper("http://10.20.24.48:8890/sparql")
	queryparent = '<http://www.semanticweb.org/btp1/stackoverflow_skill_ontology#{0}> rdfs:subClassOf ?parent'.format(skill)
	sparql.setQuery("""SELECT ?parent from <http://localhost:8890/DAV>
				WHERE{
					"""+queryparent+"""}""")
	sparql.setReturnFormat(JSON)
	results = sparql.query().convert()
	lables = results['head']['vars']
	parents = []
	# print '\n','\t  '.join(lables),'\n'
	for result in results["results"]["bindings"]:
		# print result['parent']["value"].split('#')[1]
		parents.append(result['parent']["value"].split('#')[1])
	return list(set(parents))

@app.route('/getWeights', methods=["POST"])
def getWeights():
	global parentTree, childcount, graph, vertexweight, traversed, input_skills

	input_skills = request.get_json()['query']

	graph = {}
	parentTree = {}
	childcount = {}
	vertexweight = {}

	# getting parent tree
	for k in input_skills:
		parentTree[k] = getParentTree(k)

	# build graph out of those parents
	graph = buildGraph(parentTree)

	# count number of input skills that each parent has as a descendent
	childcount = FreqDist((k for child in parentTree for k in parentTree[child].keys()))
	print '\n'.join([k+' '+str(childcount[k]) for k in childcount])
	#initialize weights for each parent. give max weight if parent belongs to input skill
	vertexweight = {k:(10 if k in input_skills else 0) for k in set(childcount.keys()+input_skills)}

	# adjust weights according to how far parent skill is from any of input skill
	for k in input_skills:
		print k
		for parent in parentTree[k].keys():
			if(vertexweight[parent]< 10-parentTree[k][parent]):
				print '  ', vertexweight[parent], parent, 10-parentTree[k][parent], ' <--'
				vertexweight[parent]=10-parentTree[k][parent]
	        # else:
	        #     print '  ', vertexweight[parent], parent, 10-parentTree[k][parent]
	traversed = set()
	for k in input_skills:
		traverse(k)

	#delete those skills with 0 weight
	zero_skill = [k for k in vertexweight if vertexweight[k]==0]
	for k in zero_skill:
		vertexweight.pop(k,None)

	# return vertexweight
	return jsonify(vertexweight)

def traverse(skill):
	global childcount, graph, vertexweight, traversed
	if skill in traversed:
		return
	traversed.update([skill])
	parents = graph[skill]
	if parents:
		parent_child_count = {parent:childcount[parent] for parent in parents}
		maxcount = max(parent_child_count.values())
		notkeep = []
		for k in parents:
			if (parent_child_count[k] is not maxcount) and (vertexweight[k] is not 10):
				notkeep.append(k)
			else:
				traverse(k)
		# print parent_child_count, maxcount
		# print notkeep
		for s in notkeep:
			vertexweight[s]=0

def buildGraph(parentTree):
	all_skills = set([k for child in parentTree for k in parentTree[child].keys()]+parentTree.keys())
	graph = {}
	print 'building graph'
	for skill in all_skills:
		print skill
		graph[skill] = getParents(skill)
		print graph[skill]
	return graph


# print '\n',getWeights(input_skills)

if __name__ == '__main__':

	app.run(host='0.0.0.0',port=5003,debug=True,threaded=True)							# debug=True: developer mode
