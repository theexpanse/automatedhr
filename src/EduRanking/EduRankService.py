from flask import Flask, request

from EduRank import EduRank

app = Flask(__name__)

@app.route('/getEduRank', methods=["POST"])
def edurankservice():
    education = request.get_json()
    eduRank = EduRank(education)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5009, debug=True, threaded=True)