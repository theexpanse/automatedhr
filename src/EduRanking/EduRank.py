import pandas as pd
import sys
import time
sys.path.append('../')
from Cleaner import Cleaner
from nltk.metrics.distance import edit_distance

def ed(a,b):
	l = max(len(a),len(b))
	e = edit_distance(a,b)
	return round(e/float(l),2)

class EduRank:
    """
    EduRank - Ranks the education of the given profile

    Input:
            1) Just education details
            2) path to the degree and institute ranking files (in case of other dataset)

    """
    id = -1
    degreepath = '../../res/Data/Degree.csv'
    institutepath = '../../res/Data/InstituteWeights/BTech.csv'
    degreeweights = pd.read_csv(degreepath,header=None,names=['degree','rank'])
    instituteweights = pd.read_csv(institutepath,header=None,names=['university','rank'])

    degree = []
    institute = []
    duration = []

    # department = [] - Yet to implement department part

    def __init__(self, *args):
        """
        :param It can have multiple parameters or single parameter
            1) Single Parameter - Dictionary containing education details
            2) Multiple Parameters - Path containing Degree Ranking, institute ranking and
                                     Dictionary containing education details
        Purpose:
         Initialises the object given education data
         Cleans and segregates the data into appropriate fields
        """
        if len(args) == 2:
            self.id = args[0]
            self.institute = args[-1]['Institute'].split('&&')
            self.degree = args[-1]['Qualification'].split('&&')
            self.duration = args[-1]['School-Duration'].split('&&')
            self.__clean()

        elif len(args) == 3:
            self.degreepath = args[0]
            self.institutepath = args[1]
            self.degreeweights = pd.DataFrame(pd.read_csv(self.degreepath))
            self.instituteweights = pd.DataFrame(pd.read_csv(self.institutepath))
            self.__clean(args[2])

        else:
            raise NotImplementedError
    def __find_institute_fromDB_edit_distance(self,candidate_institute):
        bestmatch_ed = 1
        bestmatch = None
        for university in self.instituteweights.university.tolist():   # loop through all institutes in database
            error = ed(candidate_institute,university.lower()) # find edit distance between institute-in-db and candidate's institute
            if(error < bestmatch_ed): # update best match based on lowest edit distance
                bestmatch_ed=error
                bestmatch = university
        if(bestmatch_ed<1 and bestmatch is not None): # if best match is found
            print '\n',candidate_institute,'\nED: %.2f\t'%bestmatch_ed,'\nFound Institute: ',bestmatch
        return bestmatch

    def getInstituteRank(self):
        print 'getting institute rank\n'
        ranks = {}
        for ins in self.institute: # for each candidate's institute
            bestmatch = self.__find_institute_fromDB_edit_distance(ins)
            if(bestmatch is not None):# if institute is found, add rank to dictionary
                ranks[ins] = round(self.instituteweights.loc[self.instituteweights['university']==bestmatch,'rank'].values[0],1)
        return ranks

    def __find_degree_fromDB_edit_distance(self, candidate_degree):
        bestmatch_ed = 1
        bestmatch = None
        for degree in self.degreeweights.degree.tolist():   # loop through all institutes in database
            error = ed(candidate_degree,degree.lower()) # find edit distance between institute-in-db and candidate's institute
            if(error < bestmatch_ed): # update best match based on lowest edit distance
                bestmatch_ed=error
                bestmatch = degree
        if(bestmatch_ed<1 and bestmatch is not None): # if best match is found
            print '\n',candidate_degree,'\nED: %.2f\t'%bestmatch_ed,'\nFound Degree: ',bestmatch

    def getDegreeRank(self):
        print 'getting degree rank\n'
        ranks = {}
        for d in self.degree:
            bestmatch = self.__find_degree_fromDB_edit_distance(d)
            if (bestmatch is not None):
                ranks[d] = self.degreeweights.loc[self.degree['degree']==bestmatch,'rank'].values[0]
        return ranks


    def __clean(self):
        self.institute = [inst.lower() for inst in self.institute]
        self.degree = [deg.lower() for deg in self.degree]
        clean = Cleaner()

        try:
            for i in range(len(self.duration)):
                self.institute[i] = clean.stringCleaner(self.institute[i])
                self.degree[i] = clean.stringCleaner(self.degree[i])
                print '\nInstitute: \n', self.institute[i]
        except:
            raise AttributeError

if __name__ == '__main__':
    education = {"Institute": "Don Bosco P.G. College, Acharya"
                              " Nagarjuna University && KKR & KSR Institute of Technology and Sciences",
                 "School-Duration": "2013 to 2015 && 2008 to 2012",
                 "Qualification": "M.B.A. && B.Tech in developing the application"}
    myEdu = EduRank(1, education)
    print myEdu.institute
    print myEdu.degree
    t = time.time()
    # ins_rank_dict = myEdu.getInstituteRank()
    degree_rank_dict = myEdu.getDegreeRank()
    print 'Execution time (in seconds): ',(time.time() - t)
