import random
import time
from nltk import ngrams, FreqDist
import pandas as pd
_memomask = {}

# generate n different set of hash fuctions.
def hash_function(n):
  mask = _memomask.get(n)
  if mask is None:
    random.seed(n)
    mask = _memomask[n] = random.getrandbits(32)
  def myhash(x):
    return hash(x*n) ^ mask%10**8
  return myhash

# q grams fuction, returns list of qgrams of given token 's'
def QG(s,q):
    grams = [''.join(gram) for gram in ngrams(s,q)]
    return grams

# return vector of minhash
def MH(hi,grams):
    v = []
    for h in hi:
        d = {}
        for gram in grams:
            d[gram] = h(gram)
        # print d
        min_value = min(d.itervalues())
        min_keys = [k for k in d if d[k] == min_value]
        v.append(min_keys[0])

    return v

def update_set_dict(d,k,v):
    if(d.get(k,None)==None):
        d[k] = set([v])
    else:
        d[k].update([v])
def generateETI(hi,q,df):
    ETI_dict = {}
    for row in df.itertuples():
        # print '\n',row[0],row[1][:-1].lower(),': ',row[2]
        for token in row[1][:-1].lower().split(' '):
            qgrams = QG(token,q)
            if(not qgrams):
                # print token
                update_set_dict(ETI_dict,token,row[0])
            else:
                MHt = MH(hi,qgrams)
                # print MHt,'; ',
                for q_gram in MHt:
                    update_set_dict(ETI_dict, q_gram,row[0])
    return ETI_dict

def search_entry(df,q,hi,ETI_dict,entry):
    freq_dist_of_matched_IDs = FreqDist()
    for token in entry.lower().split():
        qgrams = QG(token,q)
        if(not qgrams):
            IDs = ETI_dict.get(token,None)
            if(IDs is not None):
                freq_dist_of_matched_IDs.update(IDs)
        else:
            MHt = MH(hi,qgrams)
            for q_gram in MHt:
                IDs = ETI_dict.get(q_gram,None)
                if(IDs is not None):
                    freq_dist_of_matched_IDs.update(IDs)
    max_matching_ID = freq_dist_of_matched_IDs.max()
    print 'best match:\n',df.loc[max_matching_ID]


institutepath = '../../res/Data/InstituteWeights/BTech.csv'
instituteweights = pd.read_csv(institutepath,header=None,names=['university','rank'])

# number of hash fuctions
H = 3
# size of substring for q gram
q = 3
# generating H hash fuctions
hi = [hash_function(i) for i in range(H)]
ETI_dict = generateETI(hi,q,instituteweights)
query = 'KKR & KSR Institute of Technology Sciences'
t = time.time()
search_entry(instituteweights,q,hi,ETI_dict,query)
print 'Execution time (in seconds): ',(time.time() - t)
