from nltk.corpus import stopwords
from nltk import FreqDist
import re


class Profile:

    id = ''
    title = ''
    skills = []
    location = ''

    def __init__(self, id, title, skills, location):
        self.id = id
        self.title = title
        self.skills = skills
        self.location = location

    def cleanTitle(self):
        StopWords = stopwords.words('english')
        clean_title = lambda cl_title: set(re.findall(r'\w+', cl_title.lower()[1:])).difference(StopWords)
        self.title = map(clean_title,self.skills)
        return self.title

    def cleanCVskill(self):
        self.skills = re.split(',',self.skills)
        self.skills = filter(None, self.skills)
        PerSkillset = []
        for j in range(len(self.skills)):
            print self.skills[j]
        # return PerSkillset

title = "Senior IT Specialist && Technical Lead & Solution Architect && Senior Consultant && SOA Solutions Architect && SOA Application Architect && SOA Solutions Architect && SOA Solutions Architect && Project Coordinator && Systems Architect"
skills = "Strong experience in project development, architecture solution definition between heterogeneous applications, coordinating teams in software factory, Java development team training and SOA integration services, preparation of schedules, distribution of tasks, and has acted in pre- Sales for the submission of SOA solutions and evaluation of RFPs. (10+ years), 6 years using the SOA approach, with the practice of Oracle Fusion Middleware 11g products (Oracle Service Bus, Oracle SOA Suite, AIA and ODI). (6 years), 3 years in project implementation in business process architecture, with practice in the product Oracle Business Process Management (Oracle BPM Suite 11g, ADF and Oracle BAM). (3 years), 1 year using APIs architecture, with the practice of Oracle products Manage API, Oracle API Catalog and Oracle API Gateway. (1 year), 4 years of experience and understanding of J2EE and XML technologies (XPath, XSLT, WSDL, UDDI, SOAP, XSD, DTD). (5 years), 2 years of work with the eTOM Business Process Framework in the implementation of SOA projects insurance and telecommunications companies. (2 years), 7 years in methodologies (ITIL, TOGAF and Scrum / Kanban) in the implementation of JEE & SOA projects. (3 years), 9 years of working with and configuring one or more of the J2EE servers: JBoss, Websphere, Weblogic, Glassfish, Tomcat. (9 years), 6 years definition of architectural solutions in SAP implementation projects working on modules (FI / MM / CO / FSCD / BW) integration with PI XI / RFC in legacy systems: 4GL, MQSeries and Oracle Database to insurance company. (6 years), 8 years of development experience with Java Message Service (JMS), Hibernate and JPA. (8 years), 4 years in the development and maintenance project in COBOL language (DMSII, COBRA, edix). (4 years)"
location = "Sao Paulo, SP"
myprofile = Profile('1', title, skills, location)

myprofile.cleanCVskill()