import os

import pandas as pd
from flask import Flask, request

app = Flask(__name__)

class JobReader:
    """
    A Class to read Job Posts given the path of the files
    Methods:
    readrandom : to read Job Posts randomly
    readtitle : to read Job Posts given a title
    read
    """
    path = ''
    id = ''
    alljobs = pd.DataFrame()

    def __init__(self, path):
        self.path = path
        self.__readall()

    def __readall(self):
        files = os.listdir(self.path)
        for jobfile in files:
            tempjobs = pd.DataFrame(pd.read_csv(self.path+jobfile))
            self.alljobs = self.alljobs.append(tempjobs)
        print len(self.alljobs)

    def _readrandom(self, num):
        return self.alljobs.sample(n=num)

    def _readtitle(self, title):
        """ Method to read a job with 
        a single title given in input """

        response = self.alljobs.loc[self.alljobs['title'] == title]
        if response.empty:
            raise Exception('Job Title not Found')
        return response

    def _readtitles(self, titles):
        """ Method to read jobs with 
        multiple titles given in input """

        response = self.alljobs.loc[self.alljobs['title'].isin(titles)]
        if response.empty:
            raise Exception('Job Titles not Found')
        return response

    def _readskills(self, num):
        raise NotImplementedError("About to implement this")

path = '../../../res/Data/JobPostCSVs/'
reader = JobReader(path)
print reader._readtitle('Application Developer')
# print reader._readrandom(5)
#


@app.route('/reader', methods=['POST'])
def readerservice():
    req = request.get_json()
    req


if __name__=='__main__':
    reader = JobReader(path)
    app.run(host='0.0.0.0', port=5008, debug=True, threaded=True)
