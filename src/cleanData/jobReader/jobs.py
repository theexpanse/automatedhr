class jobs:

    title = ''
    minexp = 0
    maxexp = 0
    reqskills = []
    companyname = ''
    jobid = 0
    jobdescription = ''

    def __init__(self, title, minexp, maxexp, reqskills, companyname, jobid, jobdescription):
        self.title = title
        self.minexp = minexp
        self.maxexp = maxexp
        self.reqskills = reqskills
        self.companyname = companyname
        self.jobid = jobid
        self.jobdescription = jobdescription