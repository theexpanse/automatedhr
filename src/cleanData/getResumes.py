import sys, sh, time , json, urllib, os
import numpy as np
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import selenium.webdriver.support.ui as ui

browsers = {"firefox": webdriver.Firefox, "ie": webdriver.Ie, "chrome": webdriver.Chrome, "safari": webdriver.Safari}

def getJobTitles():
    text_file = open("it-job-titles.txt", "r")
    lines = text_file.read().split('\n')
    text_file.close()

    AllTitles = lines[:-1]
    print len(AllTitles)-1
    print 'Dim : ',len(AllTitles)-1
    # print 'Last : ',AllTitles[-1]

    # for i in AllTitles:
    #     print i,'\n'

    return AllTitles

def IndeedAuth(driverX):
    driverX.get("https://secure.indeed.com/account/login?service=my&hl=en_IN&co=IN&continue=http%3A%2F%2Fwww.indeed.co.in%2F")
    driverX.find_element_by_name("__email").send_keys("rajshah1982@yahoo.com");
    driverX.find_element_by_name("__password").send_keys("aN9cGiBY");
    driverX.find_element_by_id("loginform").submit();

def getResumes():

    IndeedAuth(driver1)
    IndeedAuth(driver2)

    # driver1.get("http://www.indeed.com/resumes?co=IN&sp=0&isid=find-resumes-IN&ikw=hometop&hl=en")
    # driver1.find_element_by_xpath("//*[@id='location_clear']").click()
    # job_box = driver1.find_element_by_xpath("//*[@id='query']")
    # job_box.send_keys(str(JobTitle))
    # job_box.send_keys(Keys.DOWN)
    # job_box.send_keys(Keys.ENTER)

    JobTitles = getJobTitles()
    print len(JobTitles)

    link = "https://www.indeed.com/resumes?q="
    cntXX = 1
    for JobTitle in JobTitles:

        if cntXX != len(JobTitles):
            cntX = 0;
            current_weblink = link+urllib.quote(JobTitle, safe="")
            next_weblink = "0"
            getCandidates(current_weblink, next_weblink, JobTitle)
        else:
            break;

        cntXX += 1

def getCandidates(current_weblink, next_weblink, JobTitle):
    print "\n*****"
    print current_weblink, next_weblink
    cntX = int(next_weblink)

    driver1.get(current_weblink)
    candy = driver1.find_elements_by_xpath("//div/div/div/div/div[2]/ol[1]/li/div/div[2]/div[1]/a")
    print "Candidate Length: ",len(candy)
    print "Next_weblink: ",next_weblink

    # if next_weblink is not "4" and next_weblink is not "-1":
    if next_weblink is not "-1":
        if len(candy) != 0:
            print "Candidate Length: ",len(candy)
            cnt = 0;

            while cnt < len(candy)-1:

                print "cnt : ",cnt
                print "\n\n\n****************LEN : ",len(candy)
                print "\nCandidate : ", candy[cnt].get_attribute('innerHTML')
                websiteX = candy[cnt].get_attribute('href')
                print "websiteX : ", candy[cnt].get_attribute('href')
                print "\nCandidates Data: \n",format(candy[cnt].text)

                driver2.get(websiteX)

                candNameX = driver2.find_elements_by_id("resume-contact")[0].text
                print "Candidate Name : ",candNameX

                sections = '; '.join(str(section.text) for section in driver2.find_elements_by_class_name("section_title"))
                sections_length = len(sections)

                s_flag = False
                ed_flag = False
                we_flag = False
                adif_flag = False
                loc_flag = False

                if 'skill' in sections.lower():
                    print "\nSkills Found"
                    s_flag = True
                else:
                    print "\nSkills Not Found!!!"
                    skillsX = "None"

                if 'education' in sections.lower():
                    print "\nEducation Found"
                    ed_flag = True
                else:
                    print "\nEducation Not Found!!!"
                    schoolsX = "None"
                    degreesX = "None"
                    school_datesX = "None"

                if 'work experience' in sections.lower():
                    print "\nWork Experience Found"
                    we_flag = True
                else:
                    print "\nWork Experience Not Found!!!"
                    jobsX = "None"
                    companiesX = "None"
                    job_datesX = "None"
                    job_descriptionsX = "None"

                if 'additional information' in sections.lower():
                    print "\nAdditional Information Found"
                    adif_flag = True
                else:
                    print "\nAdditional Information Not Found!!!"
                    additionalInfoX = "None"

                try:
                    candLocationX = driver2.find_elements_by_id("headline_location")[0].text
                    print "\nLocation Found"
                    loc_flag = True
                except:
                    print "\nLocation Not Found!!!"
                    candLocationX = "None"

                '''Resume Summary'''
                try:
                    resSummaryX = driver2.find_elements_by_id("res_summary")[0].text
                    print "\nResume Summary Found"
                except:
                    print "\nResume Summary Not Found!!!"
                    resSummaryX = "None"

                '''Work Experience'''
                if we_flag:

                    jobs = driver2.find_elements_by_class_name("work_title")

                    try:
                        companies = driver2.find_elements_by_class_name("work_company")
                        if len(companies) == 0:
                            companies = "None"
                    except:
                        companies = "None"

                    try:
                        job_dates = driver2.find_elements_by_class_name("work_dates")

                        if len(job_dates) == 0:
                            job_dates = "None"
                    except:
                        job_dates = "None"

                    try:
                        job_descriptions = driver2.find_elements_by_class_name("work_description")
                        if len(job_descriptions) == 0:
                            job_descriptions = "None"
                    except:
                        job_descriptions = "None"

                    jobsX = ""
                    companiesX = ""
                    job_datesX = ""
                    job_descriptionsX = ""

                    for i in range(0,len(jobs)):
                        if i != 0:
                            jobsX = jobsX + " && " + jobs[i].text

                            try:
                                if companies is not "None":
                                    companiesX = companiesX + " && " + companies[i].text
                                else:
                                    companiesX = companiesX + " && " + "None"

                            except:
                                companiesX = companiesX + " && " + "None"

                            try:
                                if job_dates is not "None":
                                    job_datesX = job_datesX + " && " + job_dates[i].text
                                else:
                                    job_datesX = job_datesX + " && " + "None"
                            except:
                                job_datesX = job_datesX + " && " + "None"

                            try:
                                if job_descriptions is not "None":
                                    job_descriptionsX = job_descriptionsX + " && " + job_descriptions[i].text
                                else:
                                    job_descriptionsX = job_descriptionsX + " && " + "None"
                            except:
                                job_descriptionsX = job_descriptionsX + " && " + "None"

                        else:
                            jobsX = jobs[i].text

                            if companies is not "None":
                                companiesX = companies[i].text
                            else:
                                companiesX = "None"

                            if job_dates is not "None":
                                job_datesX = job_dates[i].text
                            else:
                                job_datesX = "None"

                            if job_descriptions is not "None":
                                job_descriptionsX = job_descriptions[i].text
                            else:
                                job_descriptionsX = "None"

                    print "\nJob Title : ",jobsX
                    print "Company : ",companiesX
                    print "Job Duration : ",job_datesX
                    print "Job Description : ",job_descriptionsX

                    if jobsX == "None" and companiesX == "None" and job_datesX == "None" and job_descriptionsX== "None":
                        we_flag = False

                '''Education'''
                if ed_flag:
                    degrees = driver2.find_elements_by_class_name("edu_title")
                    schools = driver2.find_elements_by_class_name("edu_school")
                    school_dates = driver2.find_elements_by_class_name("edu_dates")

                    schoolsX = ""
                    degreesX = ""
                    school_datesX = ""

                    if len(schools) > len(degrees):
                        for i in range(0, len(schools)):
                            if i != 0:
                                schoolsX = schoolsX + " && " + schools[i].text
                                try:
                                    degreesX = degreesX + " && " + degrees[i].text
                                except:
                                    degreesX = degreesX + " && " + "None"
                                try:
                                    school_datesX = school_datesX + " && " + school_dates[i].text
                                except:
                                    school_datesX = school_datesX + " && " + "None"
                            else:
                                schoolsX = schools[i].text
                                try:
                                    degreesX = degrees[i].text
                                except:
                                    degreesX = "None"
                                try:
                                    school_datesX = school_dates[i].text
                                except:
                                    school_datesX = "None"
                    elif len(degrees) >= len(schools):
                        for i in range(0, len(degrees)):
                            if i != 0:
                                degreesX = degreesX + " && " + degrees[i].text
                                try:
                                    schoolsX = schoolsX + " && " + schools[i].text
                                except:
                                    schoolsX = schoolsX + " && " + "None"
                                try:
                                    school_datesX = school_datesX + " && " + school_dates[i].text
                                except:
                                    school_datesX = school_datesX + " && " + "None"
                            else:
                                schoolsX = schools[i].text
                                try:
                                    degreesX = degrees[i].text
                                except:
                                    degreesX = "None"
                                try:
                                    school_datesX = school_dates[i].text
                                except:
                                    school_datesX = "None"
                    else:
                        print "\nUnexpected...******"

                    print "\nSchool : ",schoolsX
                    print "Degree : ",degreesX
                    print "Duration : ",school_datesX

                    if schoolsX == "None" and degreesX == "None" and school_datesX == "None" :
                        ed_flag = False

                '''Skills'''
                if s_flag:
                    skillsX = driver2.find_elements_by_id("skills-items")[0].text
                    print "Skills : ",skillsX

                    if skillsX == "None":
                        s_flag = False

                '''Additional Info'''
                if adif_flag:
                    additionalInfoX = driver2.find_elements_by_class_name("additionalInfo-content")[0].text
                    print "Additional Info : ",additionalInfoX


                CandidateX = {
                    "Name" : candNameX,

                    "Location" : candLocationX,

                    "Education" : {

                        "Qualification" : degreesX,
                        "Institute" : schoolsX,
                        "School-Duration" : school_datesX
                        },

                    "Work-Experience" : {

                        "Job Title" : jobsX,
                        "Company" : companiesX,
                        "Job-Duration" : job_datesX,
                        "Job-Description" : job_descriptionsX
                        },

                    "Skills" : skillsX,

                    "Additional-Info" : additionalInfoX
                }

                print "#########"
                Wanted = 'Resumes/'
                Unwanted = 'Resumes/Other/OTH-'

                if s_flag and loc_flag and we_flag and ed_flag:
                    print "1: Wanted"
                    rpath = Wanted
                else:
                    print "2: Unwanted!"
                    rpath = Unwanted

                file_d = open(rpath+JobTitle+'.txt','a+')

                if os.stat(rpath+JobTitle+'.txt').st_size==0:
                    file_d.write('[')
                    file_d.write('\n')
                else:
                    file_d.write('\n\n,\n')
                file_d.write(json.dumps(CandidateX))
                file_d.close()

                cnt+=1;

                # if cnt == 4:
                #     break;

        else:
            print "\Could be Last Candidates...!!!******"
            print "\nNo Candidates Found...!!!******"
            print "Probably, an Err Page!!!******"
            cntX = -1

        try:
            next_tab = driver1.find_elements_by_xpath("//*[@id='pagination']/a[last()]")
            nextText = driver1.find_elements_by_xpath("//*[@id='pagination']/a[last()]")[0].text
            print "nextText : ", nextText

            next_weblink = driver1.find_elements_by_xpath("//*[@id='pagination']/a[last()]")[0].get_attribute('href')
            print "next_weblink : ", next_weblink


            if "Next" in nextText:
                print "Next Page is there..."
                cntX += 1
                getCandidates(next_weblink, str(cntX), JobTitle)
            else:
                print "NO Next Page...!!!"
                cntX = -1
        except:
            print "Err Page!!!******"
            cntX = -1
    else:
        print "\n****** Candidates Already******"
        cntX = -1
        ''' End of All Candidates '''
        print " End of All Candidates "

        rpaths = ['Resumes/', 'Resumes/Other/OTH-']
        for rpath in rpaths:
            file_d = open(rpath+JobTitle+'.txt','a+')
            file_d.write('\n\n')
            file_d.write(']')
            file_d.write('\n')
            file_d.close()

''' For Crawling '''
browsername = "chrome"
# if len(sys.argv) == 2:
#     browsername = sys.argv[1].lower()
browser = browsers[browsername]

driver1 = browser()
driver1.set_window_size(600, 735)
driver2 = browser()
driver2.set_window_size(600, 735)

# Convenience functions
driver1.by_id = driver1.find_element_by_id
driver1.by_xpath = driver1.find_element_by_xpath

driver2.by_id = driver2.find_element_by_id
driver2.by_xpath = driver2.find_element_by_xpath


print "Starting getResumes() Method..."
getResumes();
print "Finishing getResumes() Method..."

ch = 'y'
while ch is not "n":
    ch = raw_input("Do you want to quit?")
    print '\nYou chose : ',ch
driver1.close()
driver1.quit()
driver2.close()
driver2.quit()
