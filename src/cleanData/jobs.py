import pandas as pd
import re
from nltk.corpus import stopwords
from flask import Flask
from flask import request
import json

app = Flask(__name__)

PATH_DATA = '/home/mayank/IdeaProjects/automatedhr/res/Data/'
# PATH_DATA = '/home/axat/BTP2/automatedhr/res/Data/'


@app.route('/getjob', methods=['GET'])
def getJob():
    if request.method == 'GET':
        #jsonRes = {"ID" : [1,2,6,7]}

        jsonRes = request.get_json()
        # print jsonRes
        # JPDF = pd.DataFrame.from_records(map(json.loads, jsonRes))
        indices = jsonRes['ID']

        return 'You Posted. JSON received'

    return 'Cleaning Failed'

if __name__=='__main__':
    app.run(host='0.0.0.0', port=5007)
