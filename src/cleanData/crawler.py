import requests
from bs4 import BeautifulSoup, NavigableString
import re
import csv



# parameters: title, company name, skills, req exp, location, salary, candidate profile, education, company profile, job description, role, role catagory


print_full_job = 'Page: %d \t No. %d \
			\n Title: \t%s \n Company Name: \t%s \
			\n req Exp: \t%s \n Location: \t%s \
			\n Skills: \t%s\n Salary: %s\n Role: %s\
			\n Role Category: %s\n UG: %s\n PG: %s\n Doctorate: %s\n Job Description: %s\n'

print_job = 'Page: %d \t No. %d \
			\n Title: \t%s \n Company Name: \t%s \
			\n req Exp: \t%s \n Location: \t%s \
			\n Skills: \t%s\n'

job_parameters = ['title', 				# 0
				  'company_name',		# 1
				  'req_exp',			# 2
				  'location',			# 3
				  'skills',				# 4
				  'salary',				# 5
				  'role',				# 6
				  'role_catagory',		# 7
				  'ug',					# 8
				  'pg',					# 9
				  'doctorate',			# 10
				  'job_description']	# 11

def read_titles(f):
	f = open(f,'r')
	title_list = []
	for raw in f:
		# print '-'.join(re.findall('\w+',raw)).lower()
		title_list.append('-'.join(re.findall('\w+',raw)).lower())

	return list(sorted(set(title_list)))



def get_jobs_count(tag):
	return int(re.findall('\d+$',tag[0].string)[0])



def unique_job(jd,jp):
	d = {key:value for key,value in jp.items() if key in ['skills','title','req_exp','company_name','location']}
	if(jd.get(str(d),None)==None):
		return d
	return None



def find_jobs(job_titles,jobs_per_each_title):

	#for all jobs in file, iterate over each of them
	for job_title in job_titles:
		jobs_dict={}
		counter = 0
		page_count=1
		jobs_searched = 0
		print ('------------------------- ',job_title,'jobs -------------------------')
		csvfile = open(job_title+'.csv','w')
		writer = csv.DictWriter(csvfile, fieldnames=job_parameters)
		writer.writeheader()
		#construct url for job 
		url = 'https://www.naukri.com/'+job_title+'-jobs-in-india-'+str(page_count)
		source_code = requests.get(url)
		plain_text = source_code.text
		soup = BeautifulSoup(plain_text)
		
		jobs_count = get_jobs_count(soup.findAll('span',{'class':'cnt'}))
		print('total',jobs_count,'jobs found!')
		if(jobs_count < jobs_per_each_title):
			total_jobs=jobs_count
		else:
			total_jobs=jobs_per_each_title


		while(counter<total_jobs and jobs_searched < jobs_count):
			for job_link in soup.findAll('a',{'class':'content','target':'_blank'}):

				job_url = job_link['href']
				# print(str(counter),'  ',job_url)
				jp = get_job_parameters(job_url)
				jobs_searched += 1 
				if(jp!=0):		# if all important parameters are found then store
					uj = unique_job(jobs_dict,jp)
					skills = ','.join(jp[job_parameters[4]])
					if(uj!=None):	# if job is not found
						print ('New job:')

						jobs_dict[str(uj)]=1	# mark job as found

						counter+=1
						
						writer.writerow({job_parameters[0]:jp[job_parameters[0]],
										 job_parameters[1]:jp[job_parameters[1]],
										 job_parameters[2]:jp[job_parameters[2]],
										 job_parameters[3]:jp[job_parameters[3]],
										 job_parameters[4]:skills,
										 job_parameters[5]:jp[job_parameters[5]],
										 job_parameters[6]:jp[job_parameters[6]],
										 job_parameters[7]:jp[job_parameters[7]],
										 job_parameters[8]:jp[job_parameters[8]],
										 job_parameters[9]:jp[job_parameters[9]],
										 job_parameters[10]:jp[job_parameters[10]],
										 job_parameters[11]:jp[job_parameters[11]],})
						print (print_job%(page_count,counter,
											jp[job_parameters[0]],jp[job_parameters[1]],
											jp[job_parameters[2]],jp[job_parameters[3]],
											skills))
					else:
						print ('-----------------------------------------------------------Duplicate:')
						print (print_job%(page_count,counter,
											jp[job_parameters[0]],jp[job_parameters[1]],
											jp[job_parameters[2]],jp[job_parameters[3]],
											skills))

				if counter ==total_jobs or jobs_searched == jobs_count:
					break
			if counter == total_jobs or jobs_searched == jobs_count:
					break
			page_count+=1
			url = 'https://www.naukri.com/'+job_title+'-jobs-in-india-'+str(page_count)
			print ('new page: ',url)
			source_code = requests.get(url)
			plain_text = source_code.text
			soup = BeautifulSoup(plain_text)

		csvfile.close()

def get_job_parameters(job_url):
	job = {}
	source_code = requests.get(job_url)
	plain_text = source_code.text
	job_page = BeautifulSoup(plain_text)
	try:

		title_tag = job_page.findAll('h1',{'itemprop':'title'})[0]

		job[job_parameters[0]] = title_tag.em.next_sibling

		job[job_parameters[1]]=job_page.findAll('a',{'id':'jdCpName'})[0].string

		job[job_parameters[2]] = job_page.findAll('span',{'itemprop':'experienceRequirements'})[0].string

		job[job_parameters[3]] = job_page.findAll('div',{'itemprop':'name','class':'loc'})[0].a.string

		job[job_parameters[4]] = get_skills(job_page.findAll('div',{'class':'ksTags'})[0])

		job[job_parameters[5]] = job_page.findAll('span',{'class':'sal'})[0].em.next_sibling

		job[job_parameters[6]] = get_role(job_page.findAll('span',{'itemprop':'occupationalCategory'})[2])

		job[job_parameters[7]] = get_role(job_page.findAll('span',{'itemprop':'occupationalCategory'})[1])

		education_req = get_education(job_page.findAll('div',{'itemprop':'educationRequirements'})[0])
		job[job_parameters[8]] = education_req['ug']
		job[job_parameters[9]] = education_req['pg']
		job[job_parameters[10]] = education_req['doctorate']

		job[job_parameters[11]] = job_page.findAll('ul',{'itemprop':'description'})[0].get_text()

		return job
	except:
		return 0


def get_role(role_tag):
	# print (role_tag)
	role=''
	for item in role_tag:
		if isinstance(item,NavigableString):
			role+=item
		else:
			role+=item.string
	return role

def get_education(edu_tag):
	edu = {}
	for item in edu_tag.findAll('p'):
		edu[re.sub('\W+','',item.em.string).lower()] = item.span.string.lower()
	edu.update({key:key+' not required' for key in ['ug','doctorate','pg'] if key not in edu.keys()})
	return edu

def get_skills(div_skill_tag):
	skills = []
	for a in div_skill_tag.findAll('a'):
		# print (a.font.string)
		skills.append(a.font.string)
	return skills




job_titles=read_titles('information-technology-it-job-titles.txt')

# for i,j in enumerate(job_titles):
# 	print i,j
# print (job_titles[5:6])
find_jobs(job_titles,1000)