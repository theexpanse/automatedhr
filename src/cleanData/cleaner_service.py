import pandas as pd
import re
from nltk.corpus import stopwords
from flask import Flask, jsonify, request
import requests
import json
import random
import os

app = Flask(__name__)

# PATH_DATA = '/home/mayank/IdeaProjects/automatedhr/res/Data/'
PATH_DATA = '../../res/Data/'
JPDF = ''

StopWords = stopwords.words('english')
clean_title = lambda title: set(re.findall(r'\w+',title.lower())).difference(StopWords)
clean_jd = lambda jd: set(re.findall(r'\w+',jd.lower())).difference(StopWords)
clean_loc = lambda loc: loc.lower()
get_skillset = lambda skills_list: sorted(set(skill for skills in skills_list for skill in skills))

def cleanCVskill(skills):
    skills = re.split('[()]',skills)
    skills = filter(None, skills)
    PerSkillset = []
    PerSkillLable = []
    for j in range(len(skills)):
        if ',' not in skills[j]:
            PerSkillLable.append(skills[j])
        else:
            tmp = skills[j].split(',')
            PerSkillset.extend(tmp)
    return PerSkillset, PerSkillLable


def experienceY(expStr):
    tmpStr = str(expStr)
    expr = map(int,re.findall('\d+',tmpStr))
    return map(int,re.findall('\d+',expStr))


def getSkillList(skills):
    return [skill.lower() for skill in re.split(r'^\s*|,\s*',skills) if skill not in ['']]


def skillModify(skillList):
    if('nan' in skillList):
        skillList.remove('nan')
    for j in xrange(len(skillList)):
        skillList[j] = " ".join(skillList[j].split())
    # 	print 'before ',skillList
    for j in xrange(len(skillList)):
        skillList[j] = skillList[j].replace("/"," or ")
        skillList[j] = skillList[j].replace(" ","-")
        skillList[j] = skillList[j].replace("#","-sharp")
        skillList[j] = skillList[j].replace("+","plus")
        skillList[j] = skillList[j].replace("&","and")
        skillList[j] = re.sub(r'([\.\-#])\1{2,}', r'\1', skillList[j])

        if('.' in skillList[j]):
            if('.' == skillList[j][0]):
                if(' ' == skillList[j][1]):
                    skillList[j] = skillList[j].replace(".","dot")
                else:
                    skillList[j] = skillList[j].replace(".", "dot-")
            elif('.' == skillList[j][len(skillList[j])-1]):
                skillList[j] = skillList[j].replace(".", "-dot")
    return skillList

@app.route('/getCleanData', methods=['POST'])
def getcData():
    jsonRes = request.get_json()
    # print request.form
    indices = jsonRes['ID']
    print indices
    resPD = JPDF.ix[indices, :]
    print resPD
    # return 'hello'
    return  resPD.to_json()


@app.route('/cleaner', methods=['POST', 'GET'])
def cleaner():
    global JPDF
    if request.method == 'POST':
        jsonRes = request.get_json()
        JPDF = pd.read_json(jsonRes)
        print JPDF

        columns =  list(JPDF.columns.values)
        for i in range(len(columns)):
            columns[i] = columns[i].split('.')[0]
            print columns[i]
        # print JPDF

        JPDF.skills = map(skillModify,map(getSkillList, JPDF.skills))
        JPDF.req_exp = map(experienceY, JPDF.req_exp)
        JPDF.title = map(clean_title, JPDF.title)
        JPDF.location = map(clean_loc, JPDF.location)
        JPDF.job_description = map(clean_jd, JPDF.job_description)
        JPDF.to_pickle('JPDF')
        #calling clustering service
        # random.seed(0)
        # samples_index = random.sample(xrange(JPDF.shape[0]), 500)

        r1 = requests.get('http://localhost:5002/live')
        r2 = requests.get('http://localhost:5003/live')
        print r1.text
        print r2.text
        ind = JPDF.index
        skills = JPDF.skills.tolist()
        # ind = JPDF.iloc[samples_index,:].skills.index
        # skills = JPDF.iloc[samples_index,:].skills.tolist()
        data = {int(k):v for k,v in zip(ind,skills)}
        # print data

        url = 'http://localhost:5002/clustering'
        headers = {'Content-Type' : 'application/json'}
        r = requests.post(url, data=json.dumps(data), headers=headers)
        print r.text
        return 'You Posted. JSON received'
        
    return 'Cleaning Failed'

@app.route('/clean_client_data', methods=['POST'])
def clean_cl_data():
    client = request.get_json()
    skill_list = skillModify(getSkillList(client['skills']))
    title = clean_title(client['title'])
    return jsonify({'skills':skill_list,'title':list(title)})

if __name__=='__main__':
    global JPDF
    if os.path.isfile('JPDF'):
        JPDF = pd.read_pickle('JPDF')
        print 'file read JPDF...'


    app.run(host='0.0.0.0', port=5001, debug=True, threaded=True)
