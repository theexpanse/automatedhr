import csv, nltk
from nltk.metrics.distance import edit_distance
from nltk.corpus import stopwords
import datetime as d
import pandas as pd
from flask import Flask, request,jsonify

PATH_DATA = './'
# PATH_DATA = '../../res/Data/'
skill_graph = {}
skill_set = set()
keys=[]
StopWords = stopwords.words('english')

app = Flask(__name__)

@app.route('/live', methods=["GET"])
def life():
	return "I'm alive!, Ontology Service"

def ed(a,b):
    l = max(len(a),len(b))
    e = edit_distance(a,b)
    return round(e/float(l),2)

def loadClasses():
    csv_file = "classes_v2.csv"
    df = pd.read_csv(csv_file)
    class_column = df['Class']
    superClass_column = df['superClass']

    uniq_class_column = list(set(class_column))
    uniq_superClass_column = list(set(superClass_column))

    print ""
    print "class_column : ",len(class_column), len(uniq_class_column)
    print "duplicate class_column : ", len(class_column)-len(uniq_class_column)

    print ""
    print "superClass_column : ",len(superClass_column), len(uniq_superClass_column)
    print "duplicate superClass_column : ", len(superClass_column)-len(uniq_superClass_column)

    # print uniq_superClass_column[1:]
    columnX = uniq_class_column+uniq_superClass_column[1:]
    uniq_columnX = set(columnX)

    print ""
    print "columnX : ",len(columnX), len(uniq_columnX)
    print "duplicate columnX : ", len(columnX)-len(uniq_columnX)

    return uniq_columnX

uniq_columnX = loadClasses()
print "\n\nClasses : ",len(uniq_columnX)

def searchSkill(str1,str2,searchlist):
    Min = 1

    search_skill = str1 + '-' + str2
    matched_skill = None

    error_tolerance = 0.14

    for uniq_c in searchlist:
        ed_val = ed(uniq_c , search_skill)

        if(ed_val < Min and ed_val < error_tolerance):
            # print 'min found',uniq_c, ed_val

            Min = ed_val
            matched_skill = uniq_c

    search_skill = str2 + '-' + str1
    for uniq_c in searchlist:
        ed_val = ed(uniq_c , search_skill)

        if(ed_val < Min and ed_val < error_tolerance):
            # print 'min found',uniq_c, ed_val

            Min = ed_val
            matched_skill = uniq_c

    return matched_skill

def get_approximated_skills(input_skill):
    searchlist = []

    for uniq_c in uniq_columnX:

        if(input_skill in uniq_c):
            # print uniq_c
            searchlist.append(uniq_c)

    return searchlist

@app.route('/getMetaSkills', methods=["POST"])
def extract_skills_from_description():
    desc = request.get_json()['desc']
    sents = nltk.sent_tokenize(desc)
    extracted_skills = set()
    meta_skills = set()
    jd_tokens = set()

    for sent in sents:
        s = sent.lower()
        print '\n\nSentence:',s

        tokens = set(nltk.word_tokenize(s.lower()))
        tokens = tokens.difference(StopWords).difference(set(['.', ',', ';', ':', '-', '&', '[', ']','(',')', '$', '%', '@', '"',"'"]))

        tokens_list = list(tokens)
        print "\tKeywords : ", tokens

        if (bool(tokens & uniq_columnX)):
            skills_found = tokens.intersection(uniq_columnX)
            extracted_skills = extracted_skills.union(skills_found)

            print '\tSkills in Ontology : ',skills_found
            print '\tMeta skills : ',
            for i,token1 in enumerate(tokens_list):
                approximated_skill_list = get_approximated_skills(token1)

                if (approximated_skill_list):
                    for token2 in tokens_list[i:]:
                        extracted_skill = searchSkill(token1,token2,approximated_skill_list)

                        if(extracted_skill):
                            print extracted_skill,', ',
                            meta_skills.update([extracted_skill])

        jd_tokens=jd_tokens.union(tokens)
    Response = {'Metaskills':list(extracted_skills.union(meta_skills))}
    return jsonify(Response)

# es,ms = extract_skills_from_description("""""")
# print '\n\n----------------Results---------------- \n\nSkills in Ontology : ',es
# print 'Skills from metadata : ',ms
# str1 = 'web'
# str2 = 'service'
# searchlist = get_approximated_skills(str2)
# print 'skill list of ',str1,' is ',searchlist
# print 'matched skill is ',searchSkill(str1,str2,searchlist)
if __name__ == '__main__':

	app.run(host='0.0.0.0',port=5008,debug=True,threaded=True)							# debug=True: developer mode
