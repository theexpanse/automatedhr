# Import all libraries needed for the tutorial

# General syntax to import specific functions in a library:
##from (library) import (specific library function)
from pandas import DataFrame, read_csv
from pprint import pprint
import csv
import random
import requests
from flask import Flask, request, render_template, jsonify
# General syntax to import a library but no functions:
##import (library) as (give the library a nickname/alias)
import pandas as pd #this is how I usually import pandas
import sys #only needed to determine Python version number
import re
from os import listdir
from nltk import FreqDist
from nltk.corpus import stopwords
import math
from multiprocessing import Pool
import numpy as np
import json



app = Flask(__name__)

PATH_DATA = '../../res/Data/'
def cleanCVskill(skills):
	skills = re.split('[()]',skills)
	skills = filter(None, skills)
	PerSkillset = []
	PerSkillLable = []
	for j in range(len(skills)):
		if (',' not in skills[j]):
			PerSkillLable.append(skills[j])
		else:
			tmp = skills[j].split(',')
			PerSkillset.extend(tmp)
	return PerSkillset,PerSkillLable

def experienceY(expStr):
	tmpStr = str(expStr)
	expr = map(int,re.findall('\d+',tmpStr))
	return map(int,re.findall('\d+',expStr))

StopWords = stopwords.words('english')
clean_title = lambda title: set(re.findall(r'\w+',title.lower()[1:])).difference(StopWords)
clean_jd = lambda jd: set(re.findall(r'\w+',jd.lower())).difference(StopWords)
clean_loc = lambda loc: loc.lower()
get_skillset = lambda skills_list: sorted(set(skill for skills in skills_list for skill in skills))

def getSkillList(skills):
    return [skill.lower() for skill in re.split(r'^\s*|,\s*',skills) if skill not in ['']]

# def getSkillList(skills):
#     return [skill.lower() for skill in re.split(r'^\s|,\s',skills) if skill not in ['']]
#     return [skill.lower() for skill in re.split(r'^\s|,\s',skills)[1:]]

#----------------- Remove NaN Replace spaces by dash and . by dot -------------------
def skillModify(skillList):
    if('nan' in skillList):
        skillList.remove('nan')
    for j in xrange(len(skillList)):
        skillList[j] = " ".join(skillList[j].split())
#     	print 'before ',skillList
    for j in xrange(len(skillList)):
        skillList[j] = skillList[j].replace("/"," or ")
        skillList[j] = skillList[j].replace(" ","-")
        skillList[j] = skillList[j].replace("#","-sharp")
        skillList[j] = skillList[j].replace("+","plus")
        skillList[j] = skillList[j].replace("&","and")
        skillList[j] = re.sub(r'([\.\-#])\1{2,}', r'\1', skillList[j])


        if('.' in skillList[j]):
            if('.' == skillList[j][0]):
                if(' ' == skillList[j][1]):
                    skillList[j] = skillList[j].replace(".","dot")
                else:
                    skillList[j] = skillList[j].replace(".","dot-")
            elif('.' == skillList[j][len(skillList[j])-1]):
                skillList[j] = skillList[j].replace(".","-dot")
    return skillList



Location = './application-developer.csv'
df = pd.read_csv(Location)


@app.route('/', methods=["GET"])
def coluns():
    print 'hello world'
    return ', '.join([x for x in df.columns])

@app.route('/ids', methods=["GET"])
def indexes():
    print "Indexes"
    return ', '.join(map(str, df.index))

# @app.route('/<feature>/<int:ID>', methods=["GET"])
# def getF(feature,ID):
@app.route('/job/<int:ID>', methods=["GET", "POST"])
def getJP(ID):
    a = {v:i for (i,v) in enumerate(df.columns)}

    print "JOB : ",df.iloc[ID,:].to_json()
    return df.iloc[ID,:].to_json()


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=44000,debug=True,threaded=True)                          # debug=True: developer mode
